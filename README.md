## elasticsearch-test

Pruebas con elasticsearch

[Link instalacion elasticsearch](https://www.elastic.co/guide/en/elasticsearch/reference/current/_installation.html "Instalacion").

[Plugin Sense para Chrome](https://chrome.google.com/webstore/detail/sense-beta/lhjgkmllcaadmopgmanpapmpjgmfcfig?hl=en "Sense").


**Carga de datos de ejemplo:**

```
curl -XPOST 'localhost:9200/bank/account/_bulk?pretty' --data-binary "@accounts.json"
```


**Obtener mapping**

```
curl --silent -XGET 'http://localhost:9200/bank/_mapping/account?v&pretty'

```


**Buscar todos:**

```
curl --silent -XGET 'http://localhost:9200/bank/account/_search?v&pretty' -d '{"query": { "match_all": {} } }'
```


**Buscar por un determinado campo:**

```
curl --silent -XGET 'http://localhost:9200/bank/account/_search?v&pretty' -d '{"query": { "match": {"firstname": "Kristen"} } }'
```


**Con agregaciones:**

```
curl --silent -XGET 'localhost:9200/bank/_search?q=*&pretty' -d '
{
   "query": {
      "match_all": {}
   },
   "aggs": 
   {
       "avg_grade" : { "avg" : { "field" : "age" } }
   }
}
'
```


**Consulta con OR (con bool):**

```
curl --silent -XGET 'localhost:9200/bank/_search?pretty' -d '
{
  "query": {
    "bool": {
      "should": [
        { "match": { "address": "mill" } },
        { "match": { "address": "lane" } }
      ]
    }
  }
}
'
```


**Consulta con OR (sin bool):**

```
curl --silent -XGET 'localhost:9200/bank/_search?pretty' -d '
{
  "query": { "match_phrase": { "address": "mill lane" } }
}
'
```


**Consulta con AND:**

```
curl --silent -XGET 'localhost:9200/bank/_search?pretty' -d '
{
  "query": {
    "bool": {
      "must": [
        { "match": { "address": "mill" } },
        { "match": { "address": "lane" } }
      ]
    }
  }
}
'
```


**Consulta con AND y NOT:**

```
curl --silent -XGET 'localhost:9200/bank/_search?pretty' -d '
{
  "query": {
    "bool": {
      "must": [
        { "match": { "age": "40" } }
      ],
      "must_not": [
        { "match": { "state": "ID" } }
      ]
    }
  }
}

'
```


**Consulta con filtro:**

```
curl --silent -XGET 'localhost:9200/bank/_search?pretty' -d '
{
  "query": {
    "bool": {
      "must": { "match_all": {} },
      "filter": {
        "range": {
          "balance": {
            "gte": 20000,
            "lte": 30000
          }
        }
      }
    }
  }
}

'
```


**Borrar los datos completamente:**

```
curl -XDELETE 'localhost:9200/*'
```

