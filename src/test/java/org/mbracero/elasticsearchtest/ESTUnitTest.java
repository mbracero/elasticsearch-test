package org.mbracero.elasticsearchtest;

import static org.elasticsearch.common.xcontent.XContentFactory.jsonBuilder;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.Map;

import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHits;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.TestPropertySource;

@TestPropertySource(locations={"classpath:est.properties"})
public class ESTUnitTest {
    
    private static final String INDEX = "bank";
    private static final String TYPE = "account";
    
    Client client;
    
    @Before
    public void setUpBefore() throws UnknownHostException {
        Settings settings = Settings.settingsBuilder()
                .put("client.transport.sniff", false).build();
        
        client = TransportClient.builder()
                .settings(settings)
                .build()
                .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("localhost"), 9300));
    }
    
    @Test
    public void testCreateJson() throws IOException {
        XContentBuilder builder = jsonBuilder()
                .startObject()
                    .field("user", "kimchy")
                    .field("postDate", new Date())
                    .field("message", "trying out Elasticsearch")
                .endObject();
        String json = builder.string();
        
        Assert.assertNotEquals(json, null);
    }
    
    @Test
    public void testGet() {
        GetResponse response = client.prepareGet("bank", "account", "23").get();
        
        Map<String, Object> mapResponse = response.getSource();
        
        Assert.assertTrue(!mapResponse.isEmpty());
        Assert.assertNotEquals(mapResponse, null);
    }
    
    @Test
    public void testSearch() {
        // MatchAll on the whole cluster with all default options
        SearchResponse response = client.prepareSearch().execute().actionGet();
        SearchHits searchHits = response.getHits();
        
        Assert.assertTrue(searchHits.getTotalHits() > 0);
    }
    
    @Test
    public void testSearchInAllFileds() {
        String keyword = "*sten*";
        
        SearchResponse response = client.prepareSearch()
                .setIndices(INDEX)
                .setTypes(TYPE)
                //.addFields("age", "firstname")
                .setQuery(QueryBuilders.queryStringQuery(keyword))
                //.setPostFilter(QueryBuilders.rangeQuery("age").from(21).to(22))
                .execute()
                .actionGet();
        
        System.out.println("testSearchInAllFileds : " + response);
        /*
        // Con el uso de addFilelds
        Set<String> result = new HashSet<String>();
        for (SearchHit hit : response.getHits()) {
            String firstname = hit.field("firstname").<String>getValue();
            result.add(firstname);
        }
        */
        Assert.assertTrue(response.getHits().totalHits() > 0);
    }
    
    @Test
    public void testSearchAll() {
        SearchResponse response = client.prepareSearch(INDEX)
                .setQuery(QueryBuilders.matchAllQuery())
                //.setExplain(true)
                .execute()
                .actionGet();

        System.out.println("testSearchAll : " + response);
    }
    
    @Test
    public void testSearchRange() {
        SearchResponse response = client.prepareSearch(INDEX)
                .setTypes(TYPE)
                .setQuery(QueryBuilders.matchQuery("gender", "m"))                 // Query
                .setPostFilter(QueryBuilders.rangeQuery("age").from(12).to(22))     // Filter
                .execute()
                .actionGet();

        System.out.println("searchRange: " + response);
    }
    
    @Test
    public void testSearchBoolShould() {
    	SearchResponse response = client.prepareSearch(INDEX)
    		.setTypes(TYPE)
    		.setQuery(
    				QueryBuilders.boolQuery()
    					.should(QueryBuilders.matchQuery("address", "mill"))
    					.should(QueryBuilders.matchQuery("address", "lane"))
			)
    		.execute()
    		.actionGet();
    	
    	System.out.println("testSearchBoolShould: " + response);
    }
    

    @Test
    public void testSearchBoolMust() {
    	SearchResponse response = client.prepareSearch(INDEX)
    		.setTypes(TYPE)
    		.setQuery(
    				QueryBuilders.boolQuery()
    					.must(QueryBuilders.matchQuery("address", "mill"))
    					.must(QueryBuilders.matchQuery("address", "lane"))
			)
    		.execute()
    		.actionGet();
    	
    	System.out.println("testSearchBoolShould: " + response);
    }
    
    
    @After
    public void finalizeTest() {
        client.close();
    }
}
